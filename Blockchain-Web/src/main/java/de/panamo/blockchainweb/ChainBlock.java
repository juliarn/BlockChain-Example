package de.panamo.blockchainweb;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ChainBlock {
    private transient BlockChain blockChain;
    private long proof;
    private int index;
    private long timestamp;
    private String lastBlockHash;
    private List<Transaction> transactions;

    ChainBlock(BlockChain blockChain, long proof) {
        this.blockChain = blockChain;
        this.proof = proof;
        this.index = blockChain.getBlocks().size() + 1;
        this.timestamp = System.currentTimeMillis();
        if(!blockChain.getBlocks().isEmpty())
            this.lastBlockHash = blockChain.getLastBlock().hash();
        else
            this.lastBlockHash = "1";
        this.transactions = new ArrayList<>(this.blockChain.getUnMinedTransactions());
    }

    /**
     * @return den JSON-String des Blocks, als Sha-256-Hash.
     */

    public String hash() {
        return DigestUtils.sha256Hex(BlockChain.GSON.toJson(this));
    }

    public BlockChain getBlockChain() {
        return blockChain;
    }

    public long getProof() {
        return proof;
    }

    public int getIndex() {
        return index;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getLastBlockHash() {
        return lastBlockHash;
    }

    public Collection<Transaction> getTransactions() {
        return transactions;
    }
}
