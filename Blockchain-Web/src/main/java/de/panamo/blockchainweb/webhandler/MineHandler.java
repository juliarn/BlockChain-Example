package de.panamo.blockchainweb.webhandler;


import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import de.panamo.blockchainweb.BlockChain;

import java.io.IOException;
import java.io.OutputStream;

public class MineHandler implements HttpHandler {
    private BlockChain blockChain;

    public MineHandler(BlockChain blockChain) {
        this.blockChain = blockChain;
    }

    /**
     * Mined bei einem Request die BlockChain. Gibt als Antwort den neuen Block zurück.
     */

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        this.blockChain.mine();
        String response = BlockChain.GSON.toJson(this.blockChain.getLastBlock());
        httpExchange.sendResponseHeaders(200, response.getBytes().length);
        OutputStream outputStream = httpExchange.getResponseBody();
        outputStream.write(response.getBytes());
        outputStream.close();
    }
}
