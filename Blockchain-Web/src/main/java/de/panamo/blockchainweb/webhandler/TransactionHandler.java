package de.panamo.blockchainweb.webhandler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import de.panamo.blockchainweb.BlockChain;
import de.panamo.blockchainweb.Transaction;
import java.io.*;

public class TransactionHandler implements HttpHandler {
    private BlockChain blockChain;

    public TransactionHandler(BlockChain blockChain) {
        this.blockChain = blockChain;
    }

    /**
     * Fügt die dem Post angehängte Transaktion den nicht verarbeiteten Transaktionen der Blochchain hinzu.
     * Gibt den neuen Block als JSON zurück.
     */

    public void handle(HttpExchange httpExchange) throws IOException {
        InputStreamReader streamReader = new InputStreamReader(httpExchange.getRequestBody(), "utf-8");
        BufferedReader bufferedReader = new BufferedReader(streamReader);
        StringBuilder request = new StringBuilder();

        int currentChar;
        while ((currentChar = bufferedReader.read()) != -1)
            request.append((char) currentChar);

        streamReader.close();
        bufferedReader.close();

        this.blockChain.addTransaction(BlockChain.GSON.fromJson(request.toString(), Transaction.class));

        String response = "Die Transaktion wird dem Block "+(this.blockChain.getLastBlock().getIndex() + 1) + " hinzugefügt.";
        httpExchange.sendResponseHeaders(200, response.getBytes().length);
        OutputStream outputStream = httpExchange.getResponseBody();
        outputStream.write(response.getBytes());
        outputStream.close();
    }
}
