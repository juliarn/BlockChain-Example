package de.panamo.blockchainweb.webhandler;


import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import de.panamo.blockchainweb.BlockChain;

import java.io.IOException;
import java.io.OutputStream;

public class GetChainHandler implements HttpHandler {
    private BlockChain blockChain;

    public GetChainHandler(BlockChain blockChain) {
        this.blockChain = blockChain;
    }

    /**
     * Gibt bei einem Request die BlockChain als JSON als Antwort zurück.
     */

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        String response = this.blockChain.getChain();
        httpExchange.sendResponseHeaders(200, response.getBytes().length);
        OutputStream outputStream = httpExchange.getResponseBody();
        outputStream.write(response.getBytes());
        outputStream.close();
    }
}
